#include <hydrogen.h>
#include "test.h"

hydro_sign_keypair key_pair;
hydro_kx_keypair static_kp;

uint8_t hydro_sk[hydro_secretbox_KEYBYTES];
uint8_t hydro_hash[hydro_hash_BYTES];
uint8_t hydro_signature[hydro_sign_BYTES];
uint8_t hydro_ciphertext[hydro_secretbox_HEADERBYTES + sizeof message];

int run_hydro_init(void)
{
    hydro_init();
#ifdef TEST_ENCRYPT
    hydro_secretbox_keygen(hydro_sk);
#endif
#ifdef TEST_SIGN
    hydro_sign_keygen(&key_pair);
#endif
    return 0;
}

int run_hydro_hash(void)
{
    return hydro_hash_hash(hydro_hash, sizeof hydro_hash, message, sizeof message, context, NULL);
}

int run_hydro_kdf(void)
{
    uint8_t kdf_master_key[hydro_kdf_KEYBYTES];

    hydro_kdf_keygen(kdf_master_key);

    uint8_t subkey1[32];
    return hydro_kdf_derive_from_key(subkey1, sizeof subkey1, 1, context, kdf_master_key);
}

int run_hydro_pwhash(void)
{
    const uint8_t master_key[hydro_pwhash_MASTERKEYBYTES] = { 0 };
    const int opslimit = 10000;
    const int memlimit = 0;
    const int threads = 1;

    uint8_t derived_key[32];

    hydro_pwhash_deterministic(derived_key, sizeof derived_key, password, sizeof password,
                               context, master_key, opslimit, memlimit, threads);
    return 0;
}

int run_hydro_hydro_skgen(void)
{
    hydro_secretbox_keygen(hydro_sk);
    return 0;
}

int run_hydro_public_keygen(void)
{
    hydro_sign_keygen(&key_pair);
    return 0;
}

int run_hydro_sign(void)
{
    return hydro_sign_create(hydro_signature, message, sizeof message, context, key_pair.sk);
}

int run_hydro_verify(void)
{
    return hydro_sign_verify(hydro_signature, message, sizeof message, context, key_pair.pk);
}

int run_hydro_encrypt(void)
{
    return hydro_secretbox_encrypt(hydro_ciphertext
                                   , message, sizeof message, 0, context, hydro_sk);
}

int run_hydro_decrypt(void)
{
    char out[sizeof message];

    return hydro_secretbox_decrypt(out, hydro_ciphertext, sizeof hydro_ciphertext,
                                   0, context, hydro_sk);
}

int run_hydro_kx_keygen(void)
{
    hydro_kx_keygen(&static_kp);
    return 0;
}

int run_hydro_kx_n(void)
{
    uint8_t packet1[hydro_kx_N_PACKET1BYTES];

    hydro_kx_session_keypair session_kp;

    if (hydro_kx_n_1(&session_kp, packet1, NULL, static_kp.pk) != 0) {
        return 1;
    }

    return hydro_kx_n_2(&session_kp, packet1, NULL, &static_kp);
}

int run_hydro_kx_kk(void)
{
    uint8_t packet1[hydro_kx_KK_PACKET1BYTES];
    uint8_t packet2[hydro_kx_KK_PACKET2BYTES];

    hydro_kx_state st_client;

    hydro_kx_kk_1(&st_client, packet1, static_kp.pk, &static_kp);

    hydro_kx_session_keypair session_kp_server;
    if (hydro_kx_kk_2(&session_kp_server, packet2, packet1,
                      static_kp.pk, &static_kp) != 0) {
        return 1;
    }

    hydro_kx_session_keypair session_kp_client;
    return hydro_kx_kk_3(&st_client, &session_kp_client, packet2, static_kp.pk);
}

int run_hydro_kx_xx(void)
{
    uint8_t packet1[hydro_kx_XX_PACKET1BYTES];
    uint8_t packet2[hydro_kx_XX_PACKET2BYTES];
    uint8_t packet3[hydro_kx_XX_PACKET3BYTES];

    hydro_kx_state st_client;

    hydro_kx_xx_1(&st_client, packet1, NULL);

    hydro_kx_state st_server;
    if (hydro_kx_xx_2(&st_server, packet2, packet1, NULL, &static_kp) != 0) {
        return 1;
    }

    hydro_kx_session_keypair session_kp_client;
    if (hydro_kx_xx_3(&st_client, &session_kp_client, packet3, NULL, packet2, NULL,
                      &static_kp) != 0) {
        return 1;
    }

    hydro_kx_session_keypair session_kp_server;
    return hydro_kx_xx_4(&st_server, &session_kp_server, NULL, packet3, NULL);
}

void crypto_test_hydro(crypto_test *t)
{
    *t = (crypto_test){
        "LibHydrogen",
        run_hydro_init,
#ifdef TEST_HASH
        run_hydro_hash,
        run_hydro_pwhash,
        run_hydro_kdf,
#endif
#ifdef TEST_SIGN
        run_hydro_sign,
        run_hydro_verify,
#endif
#ifdef TEST_ENCRYPT
        run_hydro_encrypt,
        run_hydro_decrypt,
#endif
    };
}
