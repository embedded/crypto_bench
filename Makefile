APPLICATION ?= crypto_bench
BOARD       ?= native
QUIET       ?= 1

USEPKG += libhydrogen
USEPKG += tweetnacl
USEPKG += tinycrypt
USEPKG += libb2

USEMODULE += fmt
USEMODULE += xtimer
USEMODULE += benchmark

FEATURES_REQUIRED += periph_timer

# Bigger stack for TweetNaCl
CFLAGS += '-DTHREAD_STACKSIZE_MAIN=(THREAD_STACKSIZE_DEFAULT + 8192)'

# Enable or disable test functions
CFLAGS += -DTEST_HASH
CFLAGS += -DTEST_SIGN
CFLAGS += -DTEST_ENCRYPT

# Comment this out to disable code in RIOT that does safety checking
# which is not needed in a production environment but helps in the
# development process:
DEVELHELP ?= 1

include $(RIOTBASE)/Makefile.include
