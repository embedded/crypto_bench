#include <xtimer.h>
#include <irq.h>
#include <fmt.h>
#include "test.h"

void test_init(void)
{
    xtimer_init();
}

void print_usec(const uint32_t val, const size_t width)
{
    char out[width];
    size_t len = fmt_u32_dec(out, val);

    fmt_lpad(out, len, width - 3, ' ');
    print(out, width - 3);
    print_str(" µs");
}

void print_result(const char *label, benchmark_result *res)
{
    print_str(label);
    print_str(" : ");
    print_usec(res->usec, TIME_WIDTH);
    if (res->ret != 0) {
        print_str(" FAILED");
    }
    print_str("\n");
}

void print_crypto_result(crypto_result *r)
{
    print_str(r->name);
    print_str("\n");

#ifdef TEST_HASH
    print_result("hash   ", &r->hash);
    print_result("pw hash", &r->pw_hash);
    print_result("kdf    ", &r->kdf);
#endif
#ifdef TEST_SIGN
    print_result("sign   ", &r->sign);
    print_result("verify ", &r->verify);
#endif
#ifdef TEST_ENCRYPT
    print_result("encrypt", &r->encrypt);
    print_result("decrypt", &r->decrypt);
#endif
}

void print_rpad(const char *label, const size_t width)
{
    print_str(label);
    size_t pad = width - fmt_strnlen(label, width);
    while (pad--) {
        print_str(" ");
    }
}

void print_separator(const size_t labelsize, const size_t cols, const size_t colsize)
{
    for (size_t i = 0; i < labelsize; ++i) {
        print_str("-");
    }
    print_str("|");
    for (size_t i = 0; i < cols; ++i) {
        for (size_t j = 0; j <= colsize; ++j) {
            print_str("-");
        }
        print_str(":|");
    }
    print_str("\n");
}

void print_crypto_results(crypto_result *results, size_t count)
{
    char labels[FUNC_COUNT][8] = {
#ifdef TEST_HASH
        "hash   ",
        "pw hash",
        "kdf    ",
#endif
#ifdef TEST_SIGN
        "sign   ",
        "verify ",
#endif
#ifdef TEST_ENCRYPT
        "encrypt",
        "decrypt",
#endif
    };

    print_rpad("", sizeof labels[0] - 1);
    print_str(" | ");
    for (size_t i = 0; i < count; i++) {
        print_rpad(results[i].name, TIME_WIDTH);
        print_str(" | ");
    }
    print_str("\n");
    print_separator(sizeof labels[0], count, TIME_WIDTH);

    for (size_t i = 0; i < sizeof labels / sizeof labels[0]; i++) {
        print_str(labels[i]);
        print_str(" | ");
        for (size_t j = 0; j < count; j++) {
            benchmark_result *b = (benchmark_result*)(&results[j].name + 1) + i;
            if (b->ret != 0) {
                print_rpad("FAILED", TIME_WIDTH);
            }
            else if (b->usec == 0) {
                print_rpad("", TIME_WIDTH);
            }
            else {
                print_usec(b->usec, TIME_WIDTH);
            }
            print_str(" | ");
        }
        print_str("\n");
    }
}

void benchmark(benchmark_result *res, test_f func)
{
    if (func == NULL) {
        *res = (benchmark_result) { 0, 0 };
        return;
    }

    unsigned _benchmark_irqstate = irq_disable();

    xtimer_ticks32_t start = xtimer_now();
    int ret = func();
    xtimer_ticks32_t end = xtimer_now();

    res->ret = ret;
    res->usec = xtimer_usec_from_ticks(xtimer_diff(end, start));

    irq_restore(_benchmark_irqstate);
}

void crypto_benchmark(crypto_result *results, crypto_test *tests, size_t count)
{
    for (uint32_t i = 0; i < count; i++) {
        crypto_test   *t = &tests[i];
        crypto_result *r = &results[i];

        if (t->init != NULL && t->init() != 0) {
            print_str(t->name);
            print_str("INIT FAILED\n");
            continue;
        }

        r->name = t->name;
#ifdef TEST_HASH
        benchmark(&r->hash, t->hash);
        benchmark(&r->pw_hash, t->pw_hash);
        benchmark(&r->kdf, t->kdf);
#endif
#ifdef TEST_SIGN
        benchmark(&r->sign, t->sign);
        benchmark(&r->verify, t->verify);
#endif
#ifdef TEST_ENCRYPT
        benchmark(&r->encrypt, t->encrypt);
        benchmark(&r->decrypt, t->decrypt);
#endif
    }
}
