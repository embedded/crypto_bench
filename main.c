#include "test.h"
#include "hydrogen.h"
#include "tweetnacl.h"
#include "tinycrypt.h"
#include "blake2.h"

#define TEST_NACL     1
#define TEST_HYDROGEN 1

int main(void)
{
    test_init();

    crypto_result results[5];
    crypto_test tests[5];
    crypto_test_hydro(&tests[0]);
    crypto_test_tweetnacl(&tests[1]);
    crypto_test_tinycrypt(&tests[2]);
    crypto_test_blake2s(&tests[3]);
    crypto_test_blake2b(&tests[4]);

    crypto_benchmark(results, tests, sizeof(tests) / sizeof(crypto_test));
    print_crypto_results(results, sizeof(tests) / sizeof(crypto_test));

    return 0;
}
