#pragma once
#include <stdint.h>
#include <stddef.h>

#define TIME_WIDTH  16

static const int measurements = 1;
static const char context[] = "hydrogen";
static const char password[] = "hunter2";
static const char message[] = "This is a test message";

typedef int (*test_f)(void);

typedef struct CryptoTest {
    char *name;
    test_f init;
#ifdef TEST_HASH
    test_f hash, pw_hash, kdf;
#endif
#ifdef TEST_SIGN
    test_f sign, verify;
#endif
#ifdef TEST_ENCRYPT
    test_f encrypt, decrypt;
#endif
} crypto_test;

typedef struct BenchmarkResult {
    int ret;
    uint32_t usec;
} benchmark_result;

typedef struct CryptoResult {
    char *name;
#ifdef TEST_HASH
    benchmark_result hash, pw_hash, kdf;
#endif
#ifdef TEST_SIGN
    benchmark_result sign, verify;
#endif
#ifdef TEST_ENCRYPT
    benchmark_result encrypt, decrypt;
#endif
} crypto_result;

#define FUNC_COUNT ((sizeof(crypto_result) - 1)/sizeof(benchmark_result))

void test_init(void);
void print_result(const char *label, benchmark_result *res);
void print_crypto_result(crypto_result *r);
void print_crypto_results(crypto_result *r, size_t count);
void benchmark(benchmark_result *res, test_f func);
void crypto_benchmark(crypto_result *results, crypto_test *tests, size_t count);
