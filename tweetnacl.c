#include <tweetnacl.h>
#include <string.h>
#include "test.h"

typedef struct NaclM {
    unsigned char pad[crypto_secretbox_ZEROBYTES];
    unsigned char txt[sizeof message];
} nacl_m;

nacl_m msg = { { 0 }, { 0 } };

unsigned char nacl_hash[crypto_hash_BYTES];
unsigned char nacl_pk[crypto_sign_PUBLICKEYBYTES];
unsigned char nacl_sk[crypto_sign_SECRETKEYBYTES];
unsigned char nacl_sm[sizeof(msg.txt) + crypto_sign_BYTES];
unsigned char nacl_ciphertext[sizeof msg];
unsigned long long nacl_smlen;

const unsigned char nacl_key[crypto_secretbox_KEYBYTES] = { 0 };
const unsigned char nacl_nonce[crypto_secretbox_NONCEBYTES] = { 0 };

int run_tweetnacl_init(void)
{
#ifdef TEST_SIGN
    crypto_sign_keypair(nacl_pk, nacl_sk);
#endif
    memcpy(msg.txt, message, sizeof message);
    return 0;
}

int run_tweetnacl_hash(void)
{
    crypto_hash(nacl_hash, msg.txt, sizeof msg.txt);
    return 0;
}

int run_tweetnacl_sign(void)
{
    crypto_sign(nacl_sm, &nacl_smlen, msg.txt, sizeof msg.txt, nacl_sk);
    return 0;
}

int run_tweetnacl_sign_open(void)
{
    unsigned char m[sizeof nacl_sm];
    unsigned long long msglen;

    return crypto_sign_open(m, &msglen, nacl_sm, nacl_smlen, nacl_pk);
}

int run_tweetnacl_secretbox(void)
{
    return crypto_secretbox(nacl_ciphertext,
                            msg.pad, sizeof msg,
                            nacl_nonce, nacl_key);
}

int run_tweetnacl_secretbox_open(void)
{
    return crypto_secretbox_open(msg.pad,
                                 nacl_ciphertext, sizeof nacl_ciphertext,
                                 nacl_nonce, nacl_key);
}

void crypto_test_tweetnacl(crypto_test *t)
{
    *t = (crypto_test){
        "TweetNaCl",
        run_tweetnacl_init,
#ifdef TEST_HASH
        run_tweetnacl_hash,
        NULL,
        NULL,
#endif
#ifdef TEST_SIGN
        run_tweetnacl_sign,
        run_tweetnacl_sign_open,
#endif
#ifdef TEST_ENCRYPT
        run_tweetnacl_secretbox,
        run_tweetnacl_secretbox_open,
#endif
    };
}
