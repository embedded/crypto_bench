#include <tinycrypt/sha256.h>
#include <tinycrypt/aes.h>
#include "test.h"
#include "fmt.h"
#include <string.h>

uint8_t tc_msg[sizeof message];
uint8_t tc_key[TC_AES_KEY_SIZE];
uint8_t tc_ciphertext[sizeof message];

int run_tinycrypt_init(void)
{
    memcpy(tc_msg, message, sizeof message);
    return 0;
}

int run_tinycrypt_hash(void)
{
    uint8_t hash[TC_SHA256_DIGEST_SIZE];
    struct tc_sha256_state_struct ctx;

    if (tc_sha256_init(&ctx) != 1) {
        return 1;
    }
    if (tc_sha256_update(&ctx, tc_msg, sizeof tc_msg - 1) != 1) {
        return 1;
    }

    return !tc_sha256_final(hash, &ctx);
}

int run_tinycrypt_encrypt(void)
{
    struct tc_aes_key_sched_struct s;

    if (tc_aes128_set_encrypt_key(&s, tc_key) != 1) {
        return 1;
    }

    return !tc_aes_encrypt(tc_ciphertext, tc_msg, &s);
}

int run_tinycrypt_decrypt(void)
{
    struct tc_aes_key_sched_struct s;

    if (tc_aes128_set_decrypt_key(&s, tc_key) != 1) {
        return 1;
    }

    return !tc_aes_decrypt(tc_msg, tc_ciphertext, &s);
}

void crypto_test_tinycrypt(crypto_test *t)
{
    *t = (crypto_test){
        "TinyCrypt",
        run_tinycrypt_init,
#ifdef TEST_HASH
        run_tinycrypt_hash,
        NULL,
        NULL,
#endif
#ifdef TEST_SIGN
        NULL,
        NULL,
#endif
#ifdef TEST_ENCRYPT
        run_tinycrypt_encrypt,
        run_tinycrypt_decrypt,
#endif
    };
}
