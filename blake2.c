#include <blake2.h>
#include "test.h"

int run_blake2s(void)
{
    uint8_t hash[BLAKE2S_OUTBYTES];
    return blake2s(hash, message, NULL,
                   sizeof hash, sizeof message, 0);
}

int run_blake2b(void)
{
    uint8_t hash[BLAKE2B_OUTBYTES];
    return blake2b(hash, message, NULL,
                   sizeof hash, sizeof message, 0);
}

void crypto_test_blake2s(crypto_test *t)
{
    *t = (crypto_test){
        "BLAKE2s",
        NULL,
#ifdef TEST_HASH
        run_blake2s,
        NULL,
        NULL,
#endif
#ifdef TEST_SIGN
        NULL,
        NULL,
#endif
#ifdef TEST_ENCRYPT
        NULL,
        NULL,
#endif
    };
}

void crypto_test_blake2b(crypto_test *t)
{
    *t = (crypto_test){
        "BLAKE2b",
        NULL,
#ifdef TEST_HASH
        run_blake2b,
        NULL,
        NULL,
#endif
#ifdef TEST_SIGN
        NULL,
        NULL,
#endif
#ifdef TEST_ENCRYPT
        NULL,
        NULL,
#endif
    };
}

